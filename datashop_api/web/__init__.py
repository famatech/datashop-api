from starlite import Router

from .web_controller import WebIndex
from .template_config import jinja_config


WebRouteHandler = Router(path='/web', route_handlers=[
    WebIndex
])
