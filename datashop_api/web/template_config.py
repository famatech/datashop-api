from pathlib import Path

from starlite import TemplateConfig
from starlite.contrib.jinja import JinjaTemplateEngine

from datashop.utils import (
    to_yield, to_date, to_pip,
    stringify_csv, groupby_key,
    humanize_field_name
)

TEMPLATE_DIR = Path(__file__).absolute().parent.joinpath('templates')


def jinja_config() -> TemplateConfig:
    tc = TemplateConfig(
        directory=TEMPLATE_DIR,
        engine=JinjaTemplateEngine
    )
    register_template_callables(tc.engine_instance)
    return tc


def get_current_template_engine() -> JinjaTemplateEngine:
    """
    Retourne le Template Engine utilisé par l'application
    ayant la même config que celui utilisé par les Controllers
    """
    return JinjaTemplateEngine(directory=TEMPLATE_DIR)


def wrap_for_template_call(func):
    def aa(*args):
        poped = args[1:]
        return func(*poped)
    return aa


def register_template_callables(engine: JinjaTemplateEngine) -> None:
    print('Template registering Called')
    my_callables = {
        'stringify_maroc': wrap_for_template_call(stringify_csv),
        'groupby_key': wrap_for_template_call(groupby_key),
        'to_yield': wrap_for_template_call(to_yield),
        'to_pip': wrap_for_template_call(to_pip),
        'to_date': wrap_for_template_call(to_date),
        'humanize_field': wrap_for_template_call(humanize_field_name),
    }
    for keyfunc, func in my_callables.items():
        engine.register_template_callable(
            key=keyfunc,
            template_callable=func,
        )
