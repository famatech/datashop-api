from starlite import Controller, get, Template

from datashop.utils import to_date
from datashop.infrastructure import db
from datashop.core import EntityIndice, RepositoryIndice
from datashop.performance import IndicePerformance

from datashop_api.api import CacheApiIndex
from datashop_api.index_builders import build_performance_cached_index, build_periodes_index


INCLUDE = False


class WebController(Controller):
    __version__ = 'v1'


class WebIndex(WebController):

    @get(path='/', include_in_schema=INCLUDE)
    async def index(self) -> Template:
        dbsession = db.get_session()
        repo = RepositoryIndice(dbsession)
        p = IndicePerformance(repo.load_indices(code='MASIRB'), dbsession)
        rv = p.performance_periode_range(to_date('31/03'), '1:A')
        rset = rv.to_dict()['data']
        select_options = await build_performance_cached_index(dbsession, CacheApiIndex)
        # 52 Semaines # 24 Mois # 15 Ans
        periodes = build_periodes_index(52, 24, 15)
        ctx = {
            'say': 'Hello Starlite WEB',
            'perf_freq': rv,
            'records': rset,
            'select_options': select_options.collection,
            'select_periodes': periodes
        }
        print(ctx)
        return Template(name='master.html.jinja2', context=ctx)

    @get(path='/form', include_in_schema=INCLUDE)
    async def form_comparator(self) -> Template:
        dbsession = db.get_session()
        select_options = await build_performance_cached_index(dbsession, CacheApiIndex)
        # 52 Semaines # 24 Mois # 15 Ans
        periodes = build_periodes_index(52, 24, 15)
        ctx = {
            'say': 'Hello Starlite WEB',
            'select_options': select_options.collection,
            'select_periodes': periodes
        }
        return Template(name='form-comparator.html.jinja2', context=ctx)
