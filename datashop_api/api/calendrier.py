from datetime import date

from starlite import get, NotFoundException

from datashop.infrastructure import db
from datashop.calendrier import (
    MarketCalendar, CalendarPeriode, CalendarError,
    ModeCalendar, FrequenceCalendar, CalendarState
)

from .base_controller import ApiController


class ApiCalendrier(ApiController):
    path = "/calendrier"

    @get(path=['/date-reference', '/date-reference/{date_marche:date}/{periode:str}'], tags=["calendrier"])
    async def date_reference(
        self, date_marche: date, periode: str,
        mode_calendar: ModeCalendar=ModeCalendar.HEBDOMADAIRE
    ) -> CalendarPeriode:
        try:
            dbsession = db.get_session()
            dc = MarketCalendar.from_config(mode_calendar, dbsession)
            return dc.date_reference(date_marche, periode.upper())

        except CalendarError as err:
            head = f"Date calendrier indisponible: {date_marche=}, {periode=}, {mode_calendar=}"
            msg = f"{head}\n{err}"
            raise NotFoundException(msg)

    @get(path=['/dates-periodes', '/dates-periodes/{debut:date}/{fin:date}'], tags=["calendrier"])
    async def dates_periodes(
            self, debut: date, fin: date,
            periodicite: FrequenceCalendar=FrequenceCalendar.HEBDOMADAIRE,
            mode_calendar: ModeCalendar=ModeCalendar.HEBDOMADAIRE
    ) -> list[CalendarPeriode]:
        try:
            dbsession = db.get_session()
            dc = MarketCalendar.from_config(mode_calendar, dbsession)
            return dc.between(debut, fin, byfreq=periodicite)

        except CalendarError as err:
            head = f"Erreur lors de la génération des périodes entre: {debut=}, {fin=}, {periodicite=}, {mode_calendar=}"
            msg = f"{head}\n{err}"
            raise NotFoundException(msg)
        except Exception as err:
            print(err)

    @get(path=['/state', '/state/{code_indice:str}'], tags=["calendrier"])
    async def state(self, code_indice: str='MASI') -> CalendarState:
        """Retourne l'état du calendar de l'indice tel que défini par défaut.

        Returns:
            `CalendarState`: Dict de l'état du calendar
        """
        try:
            dbsession = db.get_session()
            dc = MarketCalendar.from_indice(code_indice.upper(), ModeCalendar.QUOTIDIEN, dbsession)
            return dc.getstate()

        except CalendarError as err:
            head = f"Aucun indice de code: {code_indice}"
            msg = f"{head}\n{err}"
            raise NotFoundException(msg)
