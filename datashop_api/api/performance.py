from datetime import date
from typing import Optional

from starlite import get
from starlite.exceptions import NotFoundException
from starlite.cache import SimpleCacheBackend

from datashop.infrastructure import db
from datashop.calendrier import FrequenceCalendar
from datashop.core import RepositoryIndice, RepositoryFonds
from datashop.performance import (
    ComparatorPerformance,
    PerformanceValue, PerformanceSpot,
    PerformanceCollection, FlatDelta,
    PerformanceDelta, PerformanceDeltaCollection,
)
from datashop.utils import struct_periodes_from_str

from .base_controller import ApiController, CacheApiIndex, EXPIRATION, ApiError

from datashop_api.datastructures import PerformanceDomainIndex
from datashop_api.index_builders import build_performance_cached_index

# Base des performances en point
BASE_PERFORMANCE = 100


class ApiPerformance(ApiController):
    path = "/performance"
    cache_index: SimpleCacheBackend = CacheApiIndex

    @get(path=['/', '/{code:str}'], tags=["Performance"])
    async def performance(self, code: str, date_marche: date, date_reference: Optional[date]=None, periode: Optional[str]=None) -> PerformanceSpot:
        dbsession = db.get_session()
        domain_idx: PerformanceDomainIndex = await build_performance_cached_index(
            dbsession,
            CacheApiIndex,
            expiration=EXPIRATION
        )
        periode = periode.upper() if periode else None
        if not (date_reference or periode):
            raise ApiError("Date de référence ou période requise")

        try:
            pc = domain_idx.get_by_attr('code', code.upper()).build_engine_instance(dbsession)
            if date_reference:
                rv = pc.performance(date_marche, date_reference, periode=periode)
            elif periode and not date_reference:
                rv = pc.performance_periode(date_marche, periode)

            if isinstance(rv, PerformanceValue):
                return rv.to_performance_spot()

            return rv
        except KeyError as err:
            detail = f"Le code recherché {code} ne retourne aucun moteur de calcul de la performance."
            err.add_note(detail)
            print(err)
            raise NotFoundException(detail)

    @get(path=['/periodes', '/periodes/{code:str}'], tags=["Performance"])
    async def performance_range(
        self, code: str, date_marche: date,
        date_reference: Optional[date]=None, periode: Optional[str]=None,
        frequence: FrequenceCalendar=FrequenceCalendar.HEBDOMADAIRE,
        base_point: bool=False
    ) -> PerformanceCollection:

        dbsession = db.get_session()
        domain_idx: PerformanceDomainIndex = await build_performance_cached_index(
            dbsession,
            CacheApiIndex,
            expiration=EXPIRATION
        )
        periode = periode.upper() if periode else None
        if not (date_reference or periode):
            raise ApiError("Date de référence ou période requise")

        try:
            pc = domain_idx.get_by_attr('code', code.upper()).build_engine_instance(dbsession)
            if date_reference:
                rv = pc.performance_by_freq(date_reference, date_marche, freq=frequence)

            elif periode and not date_reference:
                rv = pc.performance_periode_range(date_marche, periode, frequence)

            if base_point:
                rv = PerformanceCollection.from_performance_range(rv, base=BASE_PERFORMANCE)

            return rv.to_dict()
        except KeyError as err:
            detail = f"Le code recherché {code} ne retourne aucun moteur de calcul de la performance."
            print(err)
            raise NotFoundException(detail)

    @get(path=['/comparer', '/comparer/{code:str}/{avec:str}'], tags=["Performance/Comparateur"])
    async def comparateur_performance(self, code: str, avec: str, date_marche: date, date_reference: Optional[date]=None, periode: Optional[str]=None) -> PerformanceDelta:
        dbsession = db.get_session()
        domain_idx: PerformanceDomainIndex = await build_performance_cached_index(
            dbsession,
            CacheApiIndex,
            expiration=EXPIRATION
        )
        periode = periode.upper() if periode else None
        if not (date_reference or periode):
            raise ApiError("Date de référence ou période requise")

        try:
            perf_a = domain_idx.get_by_attr('code', code.upper()).build_engine_instance(dbsession)
            perf_b = domain_idx.get_by_attr('code', avec.upper()).build_engine_instance(dbsession)
            comparator = ComparatorPerformance(perf_a, perf_b, dbsession)

            if date_reference:
                return comparator.performance(date_marche, date_reference)

            return comparator.performance_periode(date_marche, periode)

        except KeyError as err:
            detail = f"Le code recherché {code} ou {avec} ne retourne aucun moteur de calcul de la performance."
            print(err)
            raise NotFoundException(detail)

    @get(path=['/comparer/periodes', '/comparer/periodes/{code:str}/{avec:str}'], tags=["Performance/Comparateur"])
    async def comparateur_performance_range(
        self, code: str, avec: str, date_marche: date,
        date_reference: Optional[date]=None, periode: Optional[str]=None,
        frequence: FrequenceCalendar=FrequenceCalendar.HEBDOMADAIRE,
        base_point: bool=False
    ) -> PerformanceDeltaCollection | PerformanceDeltaCollection:
        dbsession = db.get_session()
        domain_idx: PerformanceDomainIndex = await build_performance_cached_index(
            dbsession,
            CacheApiIndex,
            expiration=EXPIRATION
        )
        periode = periode.upper() if periode else None
        if not (date_reference or periode):
            raise ApiError("Date de référence ou période requise")

        try:
            perf_a = domain_idx.get_by_attr('code', code.upper()).build_engine_instance(dbsession)
            perf_b = domain_idx.get_by_attr('code', avec.upper()).build_engine_instance(dbsession)
            comparator = ComparatorPerformance(perf_a, perf_b, dbsession)

            if date_reference and base_point is False:
                rv = comparator.performance_by_freq(date_reference, date_marche, freq=frequence)

            if date_reference and base_point is True:
                rv = comparator.performance_point_by_freq(date_reference, date_marche, freq=frequence, base=BASE_PERFORMANCE)

            elif (periode and not date_reference) and base_point is False:
                rv = comparator.performance_periode_range(date_marche, periode, frequence)

            elif (periode and not date_reference) and base_point is True:
                rv = comparator.performance_point_periode(date_marche, periode, frequence)

            return rv.to_dict()
        except KeyError as err:
            detail = f"Le code recherché {code} ou {avec} ne retourne aucun moteur de calcul de la performance."
            print(err)
            raise NotFoundException(detail)

    @get(path=['/panel/comparer', '/panel/comparer/{code:str}/{avec:str}'], tags=["Performance/Comparateur"])
    async def comparateur_panel_performance(self, code: str, avec: str, date_marche: date, periodes: Optional[str]=None) -> FlatDelta:
        dbsession = db.get_session()
        domain_idx: PerformanceDomainIndex = await build_performance_cached_index(
            dbsession,
            CacheApiIndex,
            expiration=EXPIRATION
        )
        periodes = struct_periodes_from_str(periodes) if periodes else None

        try:
            perf_a = domain_idx.get_by_attr('code', code.upper()).build_engine_instance(dbsession)
            perf_b = domain_idx.get_by_attr('code', avec.upper()).build_engine_instance(dbsession)
            comparator = ComparatorPerformance(perf_a, perf_b, dbsession)
            rv = comparator.fiche_performance(date_marche, periodes)
            return rv

        except KeyError as err:
            detail = f"Le code recherché {code} ou {avec} ne retourne aucun moteur de calcul de la performance."
            print(err)
            raise NotFoundException(detail)

    # ------- TODO: Après implémentation d'Datashop Core ----------
    @get(path=['/benchmark', '/benchmark/{code:str}'], tags=["Performance/Benchmark"])
    async def benchmark_performance(self, code_fonds: str, date_marche: date, date_reference: Optional[date]=None, periode: Optional[str]=None) -> PerformanceDelta:
        pass

    @get(path=['/benchmark/periodes', '/benchmark/periodes/{code:str}'], tags=["Performance/Benchmark"])
    async def benchmark_performance_range(
        self, code: str, date_marche: date, date_reference: Optional[date]=None,
        periode: Optional[str]=None,
        frequence: FrequenceCalendar=FrequenceCalendar.HEBDOMADAIRE,
        base_point: bool=False
    ) -> PerformanceDeltaCollection | PerformanceDeltaCollection:
        pass
