from starlite import Router

from .base_controller import ApiController, CacheApiIndex

from .calendrier import ApiCalendrier
from .courbe import ApiCourbe
from .devise import ApiDevise
from .performance import ApiPerformance
from .referentiel import ApiReferentiel

from .openapi import DatashopOpenAPIController, openapi_config


ApiRouteHandler = Router(path='/api', route_handlers=[
    ApiDevise,
    ApiCourbe,
    ApiCalendrier,
    ApiPerformance,
    ApiReferentiel
])
