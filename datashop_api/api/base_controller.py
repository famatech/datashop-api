from starlite import Controller
from starlite.cache import SimpleCacheBackend


# Les Domain indexes sont des vecteurs unifiés de plusieurs SimpleIndexes,
# ils nécessitent des attributs/classes de dispatch et d'instanciaton des objets désirés
CacheApiIndex = SimpleCacheBackend()
EXPIRATION = 3600


class ApiController(Controller):
    __version__ = 'v1'


class ApiError(Exception):
    """Exception générique des API"""
