from datetime import date

from starlite import get

from datashop.infrastructure import db
from datashop.courbe import (
    CourbeBAM, ModeCourbe, PointCourbe, build_courbe_bam, build_courbe_bam_many
)

from .base_controller import ApiController


class ApiCourbe(ApiController):
    path = "/courbe"

    @get(path=['/interpoler', '/interpoler/{date_marche:date}'], tags=["Courbe"])
    async def interpolation(self, date_marche: date, periodes: list[str], mode_courbe: ModeCourbe=ModeCourbe.BAM) -> list[PointCourbe]:
        cb: CourbeBAM = build_courbe_bam(db.get_session(), date_marche, mode_courbe=mode_courbe)
        return cb.interpolate_periode_many(periodes, mode=mode_courbe)

    @get(path=['/tenors', '/tenors/{date_marche:date}'], tags=["Courbe"])
    async def tenors(self, date_marche: date, mode_courbe: ModeCourbe=ModeCourbe.BAM) -> list[PointCourbe]:
        cb: CourbeBAM = build_courbe_bam(db.get_session(), date_marche, mode_courbe=mode_courbe)
        return cb.tenors(mode=mode_courbe)
