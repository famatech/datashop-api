from typing import Any, Optional

from starlite import get, post
from starlite.exceptions import NotFoundException
from datashop.infrastructure import db
from datashop.repository import (
    ReferentielFonds,
    ReferentielTitre,
    ReferentielEmetteur,
    ReferentielSecteur,
    ReferentielPays,
    ReferentielIndices,
    ReferentielBenchmark,
    ReferentielDevise,
    ReferentielFondsMarket,
    ReferentielTitreMarket,
)

from .base_controller import ApiController


class ApiReferentiel(ApiController):
    path = '/referentiel'

    mapping = {
        'fonds': ReferentielFonds,
        'titre': ReferentielTitre,
        'emetteur': ReferentielEmetteur,
        'secteur': ReferentielSecteur,
        'pays': ReferentielPays,
        'indice': ReferentielIndices,
        'benchmark': ReferentielBenchmark,
        'devise': ReferentielDevise,
        'fonds_market': ReferentielFondsMarket,
        'titre_market': ReferentielTitreMarket,
    }

    @get(path=['/', '/{name:str}'], tags=['Referentiel'], cache=True)
    def list_referentiel(self, name: str=None) -> list[dict]:
        rv = []
        dbsession = object()
        if name:
            try:
                ref = self.mapping[name]
                rv.append({
                    'referentiel': name,
                    'champs': ref(dbsession).db_model.columns_orm()
                })
                return rv

            except KeyError:
                raise NotFoundException(
                    f'{name}: Ne correspond à aucun referentiel'
                )

        for refname, ref in self.mapping.items():
            rv.append({
                'referentiel': refname,
                'champs': ref(dbsession).db_model.columns_orm()
            })
        return rv

    @get(path=['/chercher', '/chercher/{referentiel:str}/{key:str}'], tags=['Referentiel'])
    def filter_get(self, referentiel: str, key: str, value: Any, fields_str: Optional[str]=None) -> list[dict]:
        dbsession = db.get_session()
        fields = None
        if fields_str:
            fields = [x.strip() for x in fields_str.split(',')]
        try:
            repository = self.mapping[referentiel](dbsession)
            rv = [dict(x._mapping) for x in repository.filter_get(fields=fields, filter_by={key: value})]
            return rv

        except KeyError:
            raise NotFoundException(
                f'{referentiel}: Ne correspond à aucun referentiel'
            )

    @post(path=['/chercher', '/chercher/{referentiel:str}/{key:str}'], tags=['Referentiel'])
    def filter_get_post(self, referentiel: str, key: str, value: Any, field_list: Optional[list[str]]=None) -> list[dict]:
        dbsession = db.get_session()
        try:
            repository = self.mapping[referentiel](dbsession)
            rv = [dict(x._mapping) for x in repository.filter_get(fields=field_list, filter_by={key: value})]
            return rv

        except KeyError:
            raise NotFoundException(
                f'{referentiel}: Ne correspond à aucun referentiel'
            )
