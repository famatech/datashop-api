"""Fichier de configuration du service OpenApi de Starlite

Functions:
    openapi_config_dict: Factory function pour la création du dict de configuration

        Returns:
            config_dict: dict avec les attributs ci-dessous
"""
from pydantic_openapi_schema.v3_1_0 import Contact, Server, Tag
from starlite import OpenAPIController, OpenAPIConfig


class DatashopOpenAPIController(OpenAPIController):

    path = "/api-reference"
    redoc_google_fonts = False


# ###### OpenAPI config variables #########

# ################ Basic config ######################
API_TITLE = "Datashop API"
API_VERSION = "2023.04.1"
DEFAULT_APIDOC = 'swagger'
FAKER_EXAMPLES = False

# ################ Servers Definition ######################
SRV_Datashop = Server(url="/", description="Datashop-Starlite")

# ##### TAGS (For Grouping API Endpoints) ###########
TAGS = [
    Tag(
        name="performance".title(),
        description="API des services de calcul de la performance"
    ),
    Tag(
        name="performance/comparateur".title(),
        description="API des services de calcul de la performance"
    ),
    Tag(
        name="compteur".title(),
        description="API du compteur Marché de la plateforme Datashop"
    ),
    Tag(
        name="courbe".title(),
        description="API de la courbe des taux. Interpolation et tenors"
    ),
    Tag(
        name="devise".title(),
        description="API de conversion des cours de devise. convertit un montant et extrait un cours à une date."
    ),
    Tag(
        name="referentiel".title(),
        description="API de consulation des données du référentiel. (Fonds, Indice, Titre, Emetteur, Benchmark, FondsMarket (asfim))"
    ),
]
# ########## Description de l'API Landing Page ################@
OAPI_DESCRIPTION = """
Datashop est application orientée "Data", développée en interne au sein de CDG Capital Gestion
pour répondre principalement aux besoin des reporting OPCVM.

Dans ce sens et avec le temps Datashop a pris une dimension plus large et doit répondre à d'autres besoins
vu qu'il produit déjà les chiffres nécessaires.

Chose qui a poussé à une réécriture totale d'Datashop core en **Middle-End** c-à-d entre une librairie de fonctions et de classes et une application à part entière
avec les interactions et les règles de gestion.
"""

# ####### Contact Metadata for API #################
AUTHOR = Contact(email="mfakir@cdgcapitalgestion.ma", name="M.FAKIR")


def openapi_config_dict() -> dict:
    """Factory function pour la configuration l'OpenAPIConfig pour utilisation au niveau de App instance

    Returns:
        dict: Configuration de L'openAPI Config. Maybe a subclass is more worthy to do.
    """

    rv = {
        'title': API_TITLE,
        'version': API_VERSION,
        'create_examples': FAKER_EXAMPLES,
        'openapi_controller': DatashopOpenAPIController,
        'description': OAPI_DESCRIPTION,
        'contact': AUTHOR,
        'tags': TAGS,
        'servers': [SRV_Datashop],
        'root_schema_site': DEFAULT_APIDOC
    }

    return rv


def openapi_config() -> OpenAPIConfig:
    return OpenAPIConfig(**openapi_config_dict())
