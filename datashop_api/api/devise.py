from datetime import date
from dataclasses import dataclass

from starlite import get
from starlite.exceptions import NotFoundException
from datashop.infrastructure import db
from datashop.devise import (
    DeviseTable, DeviseDateNotFound
)

from .base_controller import ApiController


@dataclass(frozen=True)
class DeviseMontant:
    date_marche: date
    montant: float
    devise_montant: str
    montant_cv: float
    devise_locale: str


@dataclass(frozen=True)
class DeviseCours:
    date_marche: date
    code_devise: str
    cours: float


class ApiDevise(ApiController):
    path = "/devise"

    @get(path=[
        '/convertir', '/convertir/{date_marche:date}/{code_devise:str}',
        '/convertir/{date_marche:date}'], tags=["Devise"]
    )
    async def conversion(self, date_marche: date, montant: float, code_devise: str) -> DeviseMontant:
        try:
            tdev: DeviseTable = DeviseTable(date_marche, db.get_session())
            montant_cv = tdev.convert_montant_devise(montant, devise_montant=code_devise)
            return DeviseMontant(date_marche, montant, code_devise, montant_cv, tdev.code_devise_locale)
        except DeviseDateNotFound as err:
            raise NotFoundException(str(err))

    @get(path=[
        '/cours', '/cours/{date_marche:date}',
        '/cours/{date_marche:date}/{code_devise:str}'], tags=["Devise"]
    )
    async def cours(self, date_marche: date, code_devise: str) -> DeviseCours:
        try:
            tdev: DeviseTable = DeviseTable(date_marche, db.get_session())
            cours = tdev.get_cours_devise(code_devise)
            return DeviseCours(date_marche, code_devise, cours)
        except DeviseDateNotFound as err:
            raise NotFoundException(str(err))
