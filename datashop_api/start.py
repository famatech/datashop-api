from starlite import Starlite

from .api import ApiRouteHandler, openapi_config
from .web import WebRouteHandler, jinja_config
from .static_files_config import static_files_config


app = Starlite(
    route_handlers=[ApiRouteHandler, WebRouteHandler],
    openapi_config=openapi_config(),
    template_config=jinja_config(),
    static_files_config=static_files_config()
)


def config_openapi_static_assets():
    oapi_controller = app.openapi_config.openapi_controller

    oapi_controller.redoc_js_url = app.url_for_static_asset('src-openapi', 'redoc.standalone.js')
    oapi_controller.swagger_css_url = app.url_for_static_asset('src-openapi', 'swagger-ui.css')
    oapi_controller.swagger_ui_bundle_js_url = app.url_for_static_asset('src-openapi', 'swagger-ui-bundle.js')
    oapi_controller.swagger_ui_standalone_preset_js_url = (
        app.url_for_static_asset('src-openapi', 'swagger-ui-standalone-preset.js'))
    oapi_controller.stoplight_elements_css_url = app.url_for_static_asset('src-openapi', 'spotlight-styles.mins.css')
    oapi_controller.stoplight_elements_js_url = (
        app.url_for_static_asset('src-openapi', 'spotlight-web-components.min.js'))


config_openapi_static_assets()

# print(app.url_for_static_asset('docs-Datashop', 'index.html'))
