from pathlib import Path
from starlite import StaticFilesConfig


SRC_PATH = Path(__file__).absolute().parent
STATIC_PATH = SRC_PATH.joinpath('static')
DOCS_PATH = SRC_PATH.joinpath('docs')


def static_files_config() -> list[StaticFilesConfig]:
    # ######## CSS and JS Resousrces #######
    # ------- OPENAPI (Redoc, Swagger, Elements) -------
    src_openapi = StaticFilesConfig(
        directories=[f"{STATIC_PATH.joinpath('src', 'openapi-offline')}"],
        path="/src-openapi", name="src-openapi")

    # ------- Htmx 1.9 -------
    src_htmx = StaticFilesConfig(
        directories=[f"{STATIC_PATH.joinpath('src', 'htmx')}"],
        path="/src-htmx", name="src-htmx")

    # ------- Jquery + Bootstrap v4.6 + Bootsrap-icons v1.10 -------
    src_bootstrap = StaticFilesConfig(
        directories=[f"{STATIC_PATH.joinpath('src', 'bootstrap')}"],
        path="/src-bootstrap", name="src-bootstrap")

    # ------- Select2 + Bootstrap 4 Theme -------
    src_select2 = StaticFilesConfig(
        directories=[f"{STATIC_PATH.joinpath('src', 'select2')}"],
        path="/src-select2", name="src-select2")

    # ------- DatePicker + Bootstrap 4 -------
    src_date_picker = StaticFilesConfig(
        directories=[f"{STATIC_PATH.joinpath('src', 'date-picker')}"],
        path="/src-date-picker", name="src-date-picker")

    # ------- Docs and Any files Static Files -------
    doc_pages = StaticFilesConfig(
        directories=[f"{DOCS_PATH}"],
        path="/docs", name="docs-Datashop", html_mode=True)

    any_docs = StaticFilesConfig(
        directories=[f"{STATIC_PATH.joinpath('refs')}"],
        path="/any-docs", name="any-docs", html_mode=True)

    return [
        src_openapi,
        src_htmx,
        src_bootstrap,
        src_select2,
        src_date_picker,
        doc_pages,
        any_docs,
    ]
