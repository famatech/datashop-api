import operator as op
from inspect import isclass
from dataclasses import dataclass, field
from typing import TypeAlias, Union, Any, Self, Callable

from sqlalchemy.orm import Session

from datashop.core import (
    EntityFonds, EntityIndice, EntityDevise, EntityIndiceComposite,
    RepositoryFonds, RepositoryIndice, RepositoryDevise, RepositoryIndiceComposite
)
from datashop.performance import (
    FondsPerformance, IndicePerformance,
    IndiceCompositePerformance, BenchmarkPerformance
)
from datashop.utils import none_sort

# -------- Performance Engine --------
PerformanceEngine: TypeAlias = Union[FondsPerformance, IndicePerformance, IndiceCompositePerformance]


@dataclass(slots=True)
class SelectOption:
    """Structure Adapt pour les options et labels des select et dropdown"""
    option: str
    label: str


@dataclass(unsafe_hash=True)
class IndexEntry:
    """
    Représente un enregistrement de l'index

    Cette structure permet une forme de mapping entre les attributs data et l'entité d'Datashop Core
    """
    uid: Any
    code: str
    description: str
    entity: Any


@dataclass(unsafe_hash=True)
class DomainIndex:
    """Represente l'index d'un domaine avec une collection d'objets `IndexEntry` accessibles via
        l'attribut `collection`.

    `DomainIndex` offre des méthodes de recherche, de filtre et de construction d'un lookup basé sur une clé
        d'attribut ou calculée suivant une fonction.
    """
    name: str
    collection: list[IndexEntry]

    def sort(self, by: str=None) -> Self:
        key_get = op.attrgetter(by)
        try:
            return self.__class__(self.name, self.collection.sort(key=key_get))
        except TypeError:
            return self.__class__(self.name, none_sort(self.collection, key=key_get))

    def get_by_attr(self, attr_name: str, attr_value: Any, *, collection_key: Callable[[str], str]=None) -> IndexEntry:
        """Cherche l'index qui vérifie `attr_name` == `attr_value`
        ou bien `collection_key(element)` == `attr_value`

        Args:

        - attr_name (`str`): le nom de l'attribut à rechercher
        - attr_value (`Any`): La valeur pour filtrer l'attribut
        - collection_key (`Callable`[[`str`], `str`], None): Fonction qui vérifie que func(el) == attr_value

        Returns:
            `IndexEntry`: L'index correspondant à `attr_value`
        """
        try:
            if collection_key:
                return [x for x in self.collection if collection_key(x) == attr_value][0]

            return [x for x in self.collection if getattr(x, attr_name) == attr_value][0]
        except KeyError:
            return None

    def get_by_entity(self, entity_cls: Any, subclass: bool=False, as_self: bool=False) -> list[IndexEntry]:
        """Retourne les `IndexEntry` de même classe que `entity_cls` ou classe fille si `subclass`est True

        Args:

        - entity_cls (`Any`): Classe d'Entity
        - subclass (`bool`, False): Si True vérifie issubclass(x.entity)
        - as_self (`bool`, False): Retourne une instance de DomainIndex avec `entity_cls.__qualname__` comme `name`

        Returns:
            `list`[`IndexEntry`] | `Self`: Collection des `IndexEntry` de même `entity_cls`
                ou `Self` avec `entity_cls.__qualname__` comme `name`

        Raises:
            `TypeError`: Si `entity_cls`n'est pas une classe
        """
        if not isclass(entity_cls):
            raise TypeError(f"DomainIndex: entity_cls doit être une classe. reçu: {entity_cls}")

        if subclass:
            coll = [x for x in self.collection if issubclass(x.entity, entity_cls)]
        else:
            coll = [x for x in self.collection if x.entity is entity_cls]

        if as_self:
            return self.__class__(entity_cls.__qualname__, coll)

        return coll

    def build_lookup(self, attr_name: str, *, key_func: Callable[[str], str]=None) -> dict[str, IndexEntry]:
        """Construit un mapping avec la valeur de `attr_name` comme clé.
            Si `key_func` renseignée attr_name et muet et n'est pas pris en compte.

        Args:

        - attr_name (`str`): Nom de l'attribut à utiliser comme clé
        - key_func (`Callable`[`str`]): Fonction pour calculer une clé de lookup.

        Returns:
            `dict`[`str`, `IndexEntry`]: Mapping avec les attributs simples ou calculés comme clé
        """
        try:
            if key_func:
                return {key_func(x): x for x in self.collection}

            return {getattr(x, attr_name): x for x in self.collection}
        except AttributeError as err:
            err.add_note(f"DomainIndex: L'attribut demandé n'existe pas: {attr_name} ou {key_func}")
            raise err


@dataclass(unsafe_hash=True)
class PerformanceIndexEntry(IndexEntry):
    performance_cls: PerformanceEngine
    performance_instance: PerformanceEngine = None

    def build_engine_instance(self, dbsession: Session) -> PerformanceEngine:
        """Instancie un Performance Engine selon le type de l'entité et ses attributs identitaires (uid, code)

        Args:

        - dbsession (`Session`): SQLA Session

        Returns:
            `PerformanceEngine`: PerformanceEngine de l'entité concernée
        """
        if self.performance_instance:
            return self.performance_instance

        if self.performance_cls is FondsPerformance:
            self.performance_instance = FondsPerformance.from_code_fonds(self.code, dbsession)

        elif self.performance_cls is IndicePerformance:
            self.performance_instance = IndicePerformance.from_code_indice(self.code, dbsession=dbsession)

        elif self.performance_cls is IndiceCompositePerformance:
            repo = RepositoryIndiceComposite(dbsession)
            icompo = repo.load_indices_composites(self.uid)
            self.performance_instance = IndiceCompositePerformance(icompo, dbsession)

        elif self.performance_cls is BenchmarkPerformance:
            pfbench = BenchmarkPerformance.from_uid(self.ui, dbsession)
            self.performance_cls = pfbench

        return self.performance_instance


@dataclass(unsafe_hash=True)
class PerformanceDomainIndex(DomainIndex):
    """Rerpésente une collection de `PerformanceIndexEntry` du domaine performance"""
    name: str = 'performance-index'
    collection: list[PerformanceIndexEntry] = field(default_factory=list)

    def build_index_engine_instance(self, dbsession: Session) -> None:
        """Instancie les Performance Engines des IndexEntry. Return True si tout se passe bien.
            à utiliser si on veut tout charger depuis le début
        """
        for entry in self.collection:
            try:
                entry.build_engine_instance(dbsession)
            except Exception:
                print(f"Erreur lors de l'instanciation de PerformanceIndex: {entry}")

        return True
