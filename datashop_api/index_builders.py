from sqlalchemy import select

from sqlalchemy.orm import Session
from starlite.cache import SimpleCacheBackend

from datashop.core import EntityFonds, EntityIndice, EntityIndiceComposite
from datashop.infrastructure import db
from datashop.performance import (
    FondsPerformance, IndicePerformance,
    IndiceCompositePerformance
)

from .datastructures import PerformanceDomainIndex, PerformanceIndexEntry, SelectOption


def build_periodes_index(nweeks: int=26, nmonths: int=12, nyears: int=10) -> tuple[SelectOption]:
    """Construit uns sélection des périodes de semaines, mois et années par incrément de 1

    Args:

    - nweeks (`int`, optional): Nombre maximum de semaines
    - nmonths (`int`, optional): Nombre maximum de mois
    - nyears (`int`, optional): Nombre maximum d'années

    Returns:
        `tuple`[`SelectOption`]: Collection des périodes
    """
    prepend = ('YTD', 'MTD', 'QTD', 'STD')
    weeks = (f"{n}:S" for n in range(1, nweeks + 1))
    months = (f"{n}:M" for n in range(1, nmonths + 1))
    years = (f"{n}:A" for n in range(1, nyears + 1))
    return tuple(SelectOption(x, x) for x in (*prepend, *weeks, *months, *years))


def build_performance_domain_index(dbsession: Session) -> PerformanceDomainIndex:

    def code_entity_select(dbmodel):
        return select(dbmodel.id, dbmodel.code, dbmodel.description).order_by(dbmodel.id)

    codes_fonds = code_entity_select(db.Fonds)
    codes_indices = code_entity_select(db.Indice)
    codes_indice_composite = code_entity_select(db.Benchmark)

    with dbsession as dbs:
        idx_fonds = [
            PerformanceIndexEntry(
                x.id, x.code, x.description, EntityFonds,
                FondsPerformance
            ) for x in dbs.execute(codes_fonds).all()
        ]
        idx_indices = [
            PerformanceIndexEntry(
                x.id, x.code, x.description, EntityIndice,
                IndicePerformance
            ) for x in dbs.execute(codes_indices).all()
        ]
        idx_indices_composite = [
            PerformanceIndexEntry(
                x.id, x.code, x.description, EntityIndiceComposite,
                IndiceCompositePerformance
            ) for x in dbs.execute(codes_indice_composite).all()
        ]

        collection: list = [*idx_fonds, *idx_indices_composite, *idx_indices]
        idx = PerformanceDomainIndex(collection=collection)
        return idx


async def build_performance_cached_index(dbsession: Session, cache_store: SimpleCacheBackend, cache_key: str=None, expiration: int=3600) -> PerformanceDomainIndex:
    cache_key = cache_key or PerformanceDomainIndex.name
    cached = await cache_store.get(cache_key)
    if cached:
        print('Cache Called')
        return cached

    idx = build_performance_domain_index(dbsession)
    await cache_store.set(cache_key, idx, expiration=expiration)
    return idx
