# Datashop-web

Datashop-web est l'application WEB 2.0 d'Datashop successeur de web_datashop.

Le projet utilise un Serveur API scalable à plusieurs workers basé sur LiteStar et une interface graphique WEB selon le contexte plus tard.

## API Server

L'interface API est structurée selon le standard Open API. La réference technique est disponible sur /api-docs


## Pages

Fonctionnalités en UI sous forme de Forms en HTML5

## Dépendances

Le nouveau Datashop-web a été révisé pour inclure de nouvelles fonctionnalités et adopter un look moderne

### Backend/MiddleEnd

    - Python Version: 3.11.5
    - WEB/API APP: Litestar >= 2.0
    - jinja2
    - uvicorn

### UI/Front End

    - Bootstrap 5
    - Select2
    - Select2-bootstrap5-theme
    - VanillaJS datepicker
    - htmx
    - Hyperscript
